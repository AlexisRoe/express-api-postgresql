import { Pool } from 'pg';
import dotenv from 'dotenv';

dotenv.config();
const db = process.env.URL_DATABASE;

export const pool = new Pool({ db });
