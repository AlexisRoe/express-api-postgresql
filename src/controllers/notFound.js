export const notFound = (_, res) => {
  res.status(404).json({ message: process.env.TEST_ENV_NOT_FOUND });
};
