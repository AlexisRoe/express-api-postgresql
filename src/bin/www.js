/* for development reasons only */
/* eslint-disable indent */
/* eslint-disable no-alert */
/* eslint-disable operator-linebreak */

import debug from 'debug';
import http from 'http';
import dotenv from 'dotenv';
import app from '../app';

dotenv.config();

const normalizePort = (val) => {
  const port = parseInt(val, 10);

  if (port.isNaN) {
    return val;
  }

  if (port >= 0) {
    return port;
  }

  return false;
};

const port = normalizePort(process.env.PORT || process.env.DEFAULT_PORT);
app.set('port', process.env.DEFAULT_PORT);

const onError = (error) => {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind =
    typeof process.env.DEFAULT_PORT === 'string'
      ? `Pipe ${process.env.DEFAULT_PORT}`
      : `Port ${process.env.DEFAULT_PORT}`;

  switch (error.code) {
    case 'EACCES':
      alert(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      alert(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
};

const server = http.createServer(app);

const onListening = () => {
  const addr = server.address();
  const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;
  debug(`Listening on ${bind}`);
};

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);
