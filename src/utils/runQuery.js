/* eslint-disable consistent-return */
/* eslint-disable no-console */
// import { createTables, insertIntoTables } from './queryFunctions';

// (async () => {
//   await createTables();
//   await insertIntoTables();
// })();

const pg = require('pg');
// or native libpq bindings
// var pg = require('pg').native

const conString = process.env.URL_DATABASE; // Can be found in the Details page
const client = new pg.Client(conString);
client.connect((err) => {
  if (err) {
    return console.error('could not connect to postgres', err);
  }
  // eslint-disable-next-line no-shadow
  client.query('SELECT NOW() AS "theTime"', (err, result) => {
    if (err) {
      return console.error('error running query', err);
    }
    console.log(result.rows[0].theTime);
    // >> output: 2018-08-23T14:02:57.117Z
    client.end();
  });
});
