"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _debug = _interopRequireDefault(require("debug"));

var _http = _interopRequireDefault(require("http"));

var _dotenv = _interopRequireDefault(require("dotenv"));

var _app = _interopRequireDefault(require("../app"));

_dotenv["default"].config();

var normalizePort = function normalizePort(val) {
  var port = parseInt(val, 10);

  if (port.isNaN) {
    return val;
  }

  if (port >= 0) {
    return port;
  }

  return false;
};

var port = normalizePort(process.env.PORT || process.env.DEFAULT_PORT);

_app["default"].set('port', process.env.DEFAULT_PORT);

var onError = function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof process.env.DEFAULT_PORT === 'string' ? "Pipe ".concat(process.env.DEFAULT_PORT) : "Port ".concat(process.env.DEFAULT_PORT);

  switch (error.code) {
    case 'EACCES':
      alert("".concat(bind, " requires elevated privileges"));
      process.exit(1);
      break;

    case 'EADDRINUSE':
      alert("".concat(bind, " is already in use"));
      process.exit(1);
      break;

    default:
      throw error;
  }
};

var server = _http["default"].createServer(_app["default"]);

var onListening = function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string' ? "pipe ".concat(addr) : "port ".concat(addr.port);
  (0, _debug["default"])("Listening on ".concat(bind));
};

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);