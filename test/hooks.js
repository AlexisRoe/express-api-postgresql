import {
    dropTables,
    createTables,
    insertIntoTables,
  } from '../src/utils/queryFunctions';
  
  // runs automatically before every test
  before(async () => {
    await createTables();
    await insertIntoTables();
  });
  
  // runs automatically after all tests are completed
  after(async () => {
    await dropTables();
  });